const streets = require('./stockholm_streets')

let toGenerate = 10
if (
    process.argv[2] &&
    parseInt(process.argv[2]) > 0 &&
    parseInt(process.argv[2]) < streets.length)
{
    toGenerate = parseInt(process.argv[2])
}

const selected = selectRandom(streets, toGenerate)
const addresses = selected.map(street => {
    return `${street.name} 1 ${street.postnr}`
})
console.log(addresses.join('\n'))
console.log()


function selectRandom (inputList, count, result = []) {
    const list = [...inputList]
    const randNum = randomNum(list.length - 1)
    const selected = list.splice(randNum, 1)

    result = result.concat(selected)
    if (result.length >= count) return result

    return selectRandom(list, count - 1, result)
}

function randomNum (max) {
    return Math.floor(0 + Math.random() * (max + 1))
}

